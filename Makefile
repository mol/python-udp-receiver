run: recvmmsg.so
	python3 udp_receiver.py

recvmmsg.so: recvmmsg.c
	gcc -o recvmmsg.so recvmmsg.c -fPIC -rdynamic -shared -O2

clean:
	rm recvmmsg.so

.PHONY: clean run
